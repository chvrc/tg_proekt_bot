import sqlite3
import time
import datetime

async def create_db():
    conn = sqlite3.connect('subs.db')
    cur = conn.cursor()
    cur.execute("""CREATE TABLE IF NOT EXISTS subs(
                    id INTEGER PRIMARY KEY,
                    uid INTEGER NOT NULL UNIQUE,
                    sub_time INTEGER);
                    """)
    conn.commit()


async def sub_add(uid):
    conn = sqlite3.connect('subs.db')
    cur = conn.cursor()
    sub_time = int(time.time() + 365 * 24 * 60 * 60)
    cur.execute("""
                INSERT OR IGNORE INTO subs(uid, sub_time) VALUES('%s', '%s');
                """ % (uid, sub_time))
    conn.commit()


async def sub_delete(uid):
    conn = sqlite3.connect('subs.db')
    cur = conn.cursor()
    cur.execute("""
                DELETE from subs
                WHERE uid='%s';
                """ % uid)
    conn.commit()


async def sub_check(uid):
    conn = sqlite3.connect('subs.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT sub_time FROM subs
                WHERE uid='%s';
                """ % uid)
    res = cur.fetchall()
    conn.commit()
    if res:
        if int(res[0][0]) > int(time.time()):
            return True
        else:
            return False
    else:
        return False


async def sub_kick():
    conn = sqlite3.connect('subs.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT uid FROM subs
                WHERE sub_time < %s;
                """ % int(time.time()))
    res = cur.fetchall()
    conn.commit()
    return res


async def sub_remain_time(uid):
    conn = sqlite3.connect('subs.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT sub_time FROM subs
                WHERE uid='%s';
                """ % uid)
    res = cur.fetchall()
    conn.commit()
    seconds = res[0][0] - int(time.time())
    return datetime.timedelta(seconds=seconds)


async def show_db():
    conn = sqlite3.connect('subs.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT * FROM subs;
                """)
    res = cur.fetchall()
    conn.commit()
    return res