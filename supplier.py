from aiogram.types.input_media import InputMediaAnimation
from load_bot import dp
import keyboards
from aiogram import types
from messages import MESSAGES


@dp.callback_query_handler(lambda c: c.data == 'supplier')
async def on_start_action(call: types.CallbackQuery):
    first_name = call.from_user.first_name
    kb = await keyboards.kb_supplier()
    await call.message.edit_media(
            media=InputMediaAnimation('CgACAgIAAxkBAAIXb2FA0VLQpqF6E84wrNenUTOFHEFOAAJEEgACQ4UAAUoHvhCdxrCiGCAE',
            caption=MESSAGES['supplier'] % first_name, parse_mode='MarkdownV2'), reply_markup=kb)
