from yoomoney import Client
from yoomoney import Quickpay
token = "4100117028579172.DB3FE693612FE0FFF1C5DCD951A2DBCA9865F73E6B63CBE720D1142DB82053F1AB69CBCE7078E1368864B732C169AA4BCC2EB05F430EC991E6ECBBB2C384116C6E3FDE437C59BCFF837AF735217F3DD083D7A8938964C87B658356184C6BEFE9C1E565ADD5AF2026C1FF8C88E692368E882ABF91CFD21C70FD00D57C0884B2BA"
client = Client(token)
user = client.account_info()
from variables import PRICE_SUB, PRICE_POSTING


async def pay_subscribe(uid):
    quickpay = Quickpay(
                receiver="4100117028579172",
                quickpay_form="shop",
                targets="Подписка на канал ProektOrdering",
                paymentType="SB",
                sum=PRICE_SUB,
                label="sub_%s" % uid
                )
    return quickpay.redirected_url


async def pay_subscribe_check(uid):
    history = client.operation_history(label="sub_%s" % uid)
    status = False
    for operation in history.operations:
        if operation.status == 'success':
            status = True
    return status


async def pay_post(uid):
    quickpay = Quickpay(
                receiver="4100117028579172",
                quickpay_form="shop",
                targets="Публикация поста на канале ProektOrdering",
                paymentType="SB",
                sum=PRICE_POSTING,
                label="post_%s" % uid
                )
    return quickpay.redirected_url


async def pay_post_check(uid):
    history = client.operation_history(label="post_%s" % uid)
    status = False
    for operation in history.operations:
        if operation.status == 'success':
            status = True
    return status
