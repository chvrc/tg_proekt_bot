from aiogram.types.input_media import InputMediaAnimation, InputMediaVideo
from aiogram.types.message import ContentType
from load_bot import dp, bot
from states import Posting
from database import sub_check
import keyboards
from aiogram import types
from aiogram.dispatcher import FSMContext
from messages import MESSAGES
from variables import post_types, CHANNEL_ID
from payments import pay_post_check


@dp.callback_query_handler(lambda c: c.data == 'posting')
async def on_start_action(call: types.CallbackQuery):
    kb = await keyboards.kb_posting()
    await call.message.edit_media(
            media=InputMediaAnimation('CgACAgIAAxkBAAIXcWFA0WoNYMiFFo9jihmz9NVGMrjzAAJGEgACQ4UAAUoKMuHTt9bBZCAE',
            caption=MESSAGES['posting'], parse_mode='MarkdownV2'), reply_markup=kb)


@dp.callback_query_handler(lambda c: c.data.startswith('postpay'))
async def on_start_action(call: types.CallbackQuery):
    kb = await keyboards.kb_post_payout(call.data)
    await call.message.edit_reply_markup(reply_markup=kb)


@dp.callback_query_handler(lambda c: c.data.startswith('postcheck'))
async def on_start_action(call: types.CallbackQuery):
    check = await pay_post_check(call.data[9:])
    if check:
        await call.message.edit_media(
                media=InputMediaAnimation('CgACAgIAAxkBAAIXhGFA08rAYpC_bGzMgnJcd0troCJ-AAJkEgACQ4UAAUrjAAFZSWWxOG0gBA',
                caption=MESSAGES['sub_posting'], parse_mode='MarkdownV2'))
        await Posting.post.set()
    else:
        await bot.answer_callback_query(callback_query_id=call.id,
                        text='Вы не оплатили публикацию поста', show_alert=True)


@dp.callback_query_handler(lambda c: c.data == 'sub_posting')
async def on_start_action(call: types.CallbackQuery):
    check = await sub_check(call.from_user.id)
    if check:
        await call.message.edit_media(
                media=InputMediaAnimation('CgACAgIAAxkBAAIXhGFA08rAYpC_bGzMgnJcd0troCJ-AAJkEgACQ4UAAUrjAAFZSWWxOG0gBA',
                caption=MESSAGES['sub_posting'], parse_mode='MarkdownV2'))
        await Posting.post.set()
    else:
        await bot.answer_callback_query(callback_query_id=call.id,
                        text='Вы не подписаны на канал ProektOrdering', show_alert=True)


@dp.message_handler(state=Posting.post)
async def posting_post(message: types.Message, state: FSMContext):
    await bot.delete_message(message.chat.id, message.message_id - 1)
    await bot.delete_message(message.chat.id, message.message_id)
    async with state.proxy() as data:
        data['post'] = message.text
    kb = await keyboards.kb_post_type()
    await message.reply("К какому типу сообщений относится Ваш пост?",
                    reply_markup=kb, reply=False)
    await Posting.post_type.set()

@dp.callback_query_handler(state=Posting.post_type)
async def post_type(call: types.CallbackQuery, state: FSMContext):
    await bot.delete_message(call.message.chat.id, call.message.message_id)
    async with state.proxy() as data:
        data['post_type'] = call.data
    text = """Для привлечения внимания дополните пост изображением, видео, документом или GIF, \
для этого направьте картинку или нажмите кнопку "Пропустить\""""
    kb = await keyboards.kb_skip()
    await bot.send_message(call.message.chat.id, text, reply_markup=kb)
    await Posting.post_file.set()


@dp.message_handler(state=Posting.post_type, content_types=ContentType.ANY)
async def posting_post(message: types.Message):
    await bot.delete_message(message.chat.id, message.message_id)


@dp.message_handler(state=Posting.post_file, content_types=ContentType.PHOTO)
async def post_file(message: types.Message, state: FSMContext):
    await bot.delete_message(message.chat.id, message.message_id - 1)
    await bot.delete_message(message.chat.id, message.message_id)
    async with state.proxy() as data:
        post = data['post']
        post_type = data['post_type']
    post_file = message.photo[-1].file_id
    msg = "@%s\n%s\n\n#%s" % (message.from_user.username, post, post_types[post_type])
    await bot.send_photo(chat_id=CHANNEL_ID, photo=post_file, caption=msg)
    kb = await keyboards.kb_exit()
    await bot.send_animation(chat_id=message.chat.id,
                            animation='CgACAgIAAxkBAAIXdWFA0eathCMdRkM9KWYok6MfShFWAAJMEgACQ4UAAUq8I5cSaQhMBCAE',
                            caption='Пост успешно опубликован\!', reply_markup=kb, parse_mode='MarkdownV2')
    await state.finish()


@dp.message_handler(state=Posting.post_file, content_types=ContentType.ANIMATION)
async def post_file(message: types.Message, state: FSMContext):
    await bot.delete_message(message.chat.id, message.message_id - 1)
    await bot.delete_message(message.chat.id, message.message_id)
    async with state.proxy() as data:
        post = data['post']
        post_type = data['post_type']
    post_file = message.animation.file_id
    msg = "@%s\n%s\n\n#%s" % (message.from_user.username, post, post_types[post_type])
    await bot.send_animation(chat_id=CHANNEL_ID, animation=post_file, caption=msg)
    kb = await keyboards.kb_exit()
    await bot.send_animation(chat_id=message.chat.id,
                            animation='CgACAgIAAxkBAAIXdWFA0eathCMdRkM9KWYok6MfShFWAAJMEgACQ4UAAUq8I5cSaQhMBCAE',
                            caption='Пост успешно опубликован\!', reply_markup=kb, parse_mode='MarkdownV2')
    await state.finish()


@dp.message_handler(state=Posting.post_file, content_types=ContentType.VIDEO)
async def post_file(message: types.Message, state: FSMContext):
    await bot.delete_message(message.chat.id, message.message_id - 1)
    await bot.delete_message(message.chat.id, message.message_id)
    async with state.proxy() as data:
        post = data['post']
        post_type = data['post_type']
    post_file = message.video.file_id
    msg = "@%s\n%s\n\n#%s" % (message.from_user.username, post, post_types[post_type])
    await bot.send_video(chat_id=CHANNEL_ID, video=post_file, caption=msg)
    kb = await keyboards.kb_exit()
    await bot.send_animation(chat_id=message.chat.id,
                            animation='CgACAgIAAxkBAAIXdWFA0eathCMdRkM9KWYok6MfShFWAAJMEgACQ4UAAUq8I5cSaQhMBCAE',
                            caption='Пост успешно опубликован\!', reply_markup=kb, parse_mode='MarkdownV2')
    await state.finish()


@dp.message_handler(state=Posting.post_file, content_types=ContentType.DOCUMENT)
async def post_file(message: types.Message, state: FSMContext):
    await bot.delete_message(message.chat.id, message.message_id - 1)
    await bot.delete_message(message.chat.id, message.message_id)
    async with state.proxy() as data:
        post = data['post']
        post_type = data['post_type']
    post_file = message.document.file_id
    msg = "@%s\n%s\n\n#%s" % (message.from_user.username, post, post_types[post_type])
    await bot.send_document(chat_id=CHANNEL_ID, document=post_file, caption=msg)
    kb = await keyboards.kb_exit()
    await bot.send_animation(chat_id=message.chat.id,
                            animation='CgACAgIAAxkBAAIXdWFA0eathCMdRkM9KWYok6MfShFWAAJMEgACQ4UAAUq8I5cSaQhMBCAE',
                            caption='Пост успешно опубликован\!', reply_markup=kb, parse_mode='MarkdownV2')
    await state.finish()


@dp.message_handler(state=Posting.post_file, content_types=ContentType.ANY)
async def post_file(message: types.Message, state: FSMContext):
    await bot.delete_message(message.chat.id, message.message_id - 1)
    await bot.delete_message(message.chat.id, message.message_id)
    if message.text == 'Пропустить':
        async with state.proxy() as data:
            post = data['post']
            post_type = data['post_type']
        msg = "@%s\n%s\n\n#%s" % (message.from_user.username, post, post_types[post_type])
        await bot.send_message(CHANNEL_ID, msg)
        kb = await keyboards.kb_exit()
        await bot.send_animation(chat_id=message.chat.id,
                            animation='CgACAgIAAxkBAAIXdWFA0eathCMdRkM9KWYok6MfShFWAAJMEgACQ4UAAUq8I5cSaQhMBCAE',
                            caption='Пост успешно опубликован\!', reply_markup=kb, parse_mode='MarkdownV2')
        await state.finish()
    else:
        kb = await keyboards.kb_skip()
        await message.reply("Ошибка! Пришлите изображение, видео, документ, GIF или нажмите кнопку \"Пропустить\"", 
                            reply=False, reply_markup=kb)
    
    
