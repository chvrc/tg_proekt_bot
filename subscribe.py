from variables import CHANNEL_ID
from aiogram.types.input_media import InputMediaAnimation, InputMediaVideo
from load_bot import dp
import database
import keyboards
from aiogram import types
from messages import MESSAGES
from payments import pay_subscribe_check


@dp.callback_query_handler(lambda c: c.data == 'subscribe')
async def on_start_action(call: types.CallbackQuery):
    kb = await keyboards.kb_subscribe()
    await call.message.edit_media(
            media=InputMediaAnimation('CgACAgIAAxkBAAIXc2FA0cyWWDPsgrAgjOVQ6SQWD39iAAJKEgACQ4UAAUpFupOUIirfPCAE',
            caption=MESSAGES['subscribe'], parse_mode='MarkdownV2'), reply_markup=kb)


@dp.callback_query_handler(lambda c: c.data.startswith('subpay'))
async def on_start_action(call: types.CallbackQuery):
    kb = await keyboards.kb_sub_payout(call.data)
    await call.message.edit_reply_markup(reply_markup=kb)


@dp.callback_query_handler(lambda c: c.data == 'sub_benefits')
async def on_start_action(call: types.CallbackQuery):
    kb = await keyboards.kb_sub_benefits()
    await call.message.edit_media(
            media=InputMediaVideo('BAACAgIAAxkBAAIXe2FA0zhftDrI5Zb4kF8PsYGyFc8NAAJeEgACQ4UAAUoGsfyB-Z9JPSAE',
            caption=MESSAGES['sub_benefits'], parse_mode='MarkdownV2'), reply_markup=kb)


@dp.callback_query_handler(lambda c: c.data.startswith('subcheck'))
async def on_start_action(call: types.CallbackQuery):
    check = await pay_subscribe_check(call.data[8:])
    if check:
        await database.sub_add(call.from_user.id)
        await dp.bot.unban_chat_member(chat_id=CHANNEL_ID, user_id=call.from_user.id, only_if_banned=True)
        kb = await keyboards.kb_sub_success()
        await call.message.edit_media(
            media=InputMediaVideo('CgACAgIAAxkBAAIXd2FA0pc_Jnodn7vfFuMq5l0G-vEUAAJSEgACQ4UAAUpaY4d3fioJGCAE',
            caption='Поздравляем\, Ваша оплата подписки на канал ProektOrdering прошла успешно\!',
            parse_mode='MarkdownV2'), reply_markup=kb)
    else:
        await dp.bot.answer_callback_query(callback_query_id=call.id,
                        text='Вы не подписаны', show_alert=True)
