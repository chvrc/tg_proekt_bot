from aiogram.dispatcher.filters.state import State, StatesGroup

class Posting(StatesGroup):
    post = State()
    post_type = State()
    post_file = State()