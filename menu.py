from aiogram.types.input_media import InputMediaAnimation
from aiogram.types.message import ContentType
from load_bot import ADMIN_ID, dp, bot
from contextlib import suppress
import database
import keyboards
from aiogram import types
from aiogram.dispatcher.filters import Command
from aiogram.utils.exceptions import MessageCantBeDeleted, MessageToDeleteNotFound
from messages import MESSAGES
from variables import CHANNEL_ID


@dp.callback_query_handler(lambda c: c.data == 'start')
async def on_start_action(call: types.CallbackQuery):
    uid = call.from_user.id
    first_name = call.from_user.first_name
    check = await database.sub_check(uid)
    kb = await keyboards.kb_start()
    if check:
        time_remain = await database.sub_remain_time(uid)
        msg = MESSAGES['start'] % first_name + '\n*\[Вы подписаны\]*\n_Оставшееся время подписки\:_ %s' % time_remain
    else:
        msg = MESSAGES['start'] % first_name
    await call.message.edit_media(
            media=InputMediaAnimation('CgACAgIAAxkBAAIXaWFA0E8zv4oihgTbZxNUFPyz0U2pAAJBEgACQ4UAAUoI5JUCKysWLSAE',
            caption=msg, parse_mode='MarkdownV2'), reply_markup=kb)


@dp.message_handler(Command('posting'))
async def on_start(message: types.Message):
    with suppress(MessageCantBeDeleted, MessageToDeleteNotFound):
        await bot.delete_message(message.chat.id, message.message_id)
        await bot.delete_message(message.chat.id, message.message_id - 1)
    kb = await keyboards.kb_posting()
    await bot.send_animation(chat_id=message.chat.id,
                            animation='CgACAgIAAxkBAAIXcWFA0WoNYMiFFo9jihmz9NVGMrjzAAJGEgACQ4UAAUoKMuHTt9bBZCAE',
                            caption=MESSAGES['posting'], reply_markup=kb, parse_mode='MarkdownV2')


@dp.message_handler()
async def on_start(message: types.Message):
    with suppress(MessageCantBeDeleted, MessageToDeleteNotFound):
        await bot.delete_message(message.chat.id, message.message_id)
        await bot.delete_message(message.chat.id, message.message_id - 1)
    uid = message.from_user.id
    first_name = message.from_user.first_name
    check = await database.sub_check(uid)
    kb = await keyboards.kb_start()
    if check:
        time_remain = await database.sub_remain_time(uid)
        msg = MESSAGES['start'] % first_name + '\n*\[Вы подписаны\]*\n_Оставшееся время подписки\:_ %s' % time_remain
    else:
        msg = MESSAGES['start'] % first_name
    await bot.send_animation(chat_id=message.chat.id,
                            animation='CgACAgIAAxkBAAIXaWFA0E8zv4oihgTbZxNUFPyz0U2pAAJBEgACQ4UAAUoI5JUCKysWLSAE',
                            caption=msg, reply_markup=kb, parse_mode='MarkdownV2')