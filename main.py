from set_bot_commands import set_default_commands
from aiogram.utils import executor
from load_bot import bot, loop
from database import create_db


async def on_shutdown(dp):
    await bot.close()


async def on_startup(dispatcher):
    await set_default_commands(dispatcher)
    await create_db()


if __name__ == "__main__":
    from menu import dp
    from customer import dp
    from designer import dp
    from supplier import dp
    from subscribe import dp
    from posting import dp
    executor.start_polling(dp, loop=loop,on_shutdown=on_shutdown, on_startup=on_startup)
