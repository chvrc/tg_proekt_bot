from aiogram.types import (InlineKeyboardMarkup, InlineKeyboardButton, 
                            ReplyKeyboardMarkup, KeyboardButton)
from variables import CHANNEL_ID, PRICE_POSTING
from payments import pay_subscribe, pay_post
import time
from load_bot import bot


async def kb_start():
    bt1 = InlineKeyboardButton('Заказчик👥', callback_data='customer')
    bt2 = InlineKeyboardButton('Проектировщик👨‍💻👩‍💻', callback_data='designer')
    bt3 = InlineKeyboardButton('Поставщик оборудования®', callback_data='supplier')
    bt4 = InlineKeyboardButton('Пользовательское соглашение', url='https://telegra.ph/Soglashenie-ob-ispolzovanii-v-telegramm-chat-bota-ProektOrderingBot-dalee-po-tekstu--Soglashenie-09-01')
    kb = InlineKeyboardMarkup()
    kb.row(bt1)
    kb.row(bt2)
    kb.row(bt3)
    kb.row(bt4)
    return kb


async def kb_exit():
    bt1 = InlineKeyboardButton('🏠Вернуться в главное меню', callback_data='start')
    kb = InlineKeyboardMarkup()
    kb.row(bt1)
    return kb


async def kb_posting():
    bt1 = InlineKeyboardButton('💰Да, перейти к оплате %sР💴' % PRICE_POSTING,
                        callback_data='postpay%s' % int(time.time()))
    bt2 = InlineKeyboardButton('✍Пост от подписчика ProektOrdering🆓', callback_data='sub_posting')
    bt3 = InlineKeyboardButton('💰Подписаться на канал ProektOrdering🖋', callback_data='subscribe')
    bt4 = InlineKeyboardButton('🏠Вернуться в главное меню', callback_data='start')
    kb = InlineKeyboardMarkup()
    kb.row(bt1)
    kb.row(bt2)
    kb.row(bt3)
    kb.row(bt4)
    return kb


async def kb_customer():
    bt1 = InlineKeyboardButton('💰Разместить пост📝', callback_data='posting')
    bt2 = InlineKeyboardButton('💰Подписаться на канал ProektOrdering🖋', callback_data='subscribe')
    bt3 = InlineKeyboardButton('⬅️Назад', callback_data='start')
    kb = InlineKeyboardMarkup()
    kb.row(bt1)
    kb.row(bt2)
    kb.row(bt3)
    return kb


async def kb_designer():
    bt1 = InlineKeyboardButton('💰Подписаться на канал ProektOrdering🖋', callback_data='subscribe')
    bt2 = InlineKeyboardButton('💰Разместить резюме/вакансию📝', callback_data='posting')
    bt3 = InlineKeyboardButton('ℹ️Новостной канал ProektPlus🎏', url='https://t.me/proekt_plus')
    bt4 = InlineKeyboardButton('⬅️Назад', callback_data='start')
    kb = InlineKeyboardMarkup()
    kb.row(bt1)
    kb.row(bt2)
    kb.row(bt3)
    kb.row(bt4)
    return kb


async def kb_supplier():
    bt1 = InlineKeyboardButton('💰Подписаться на канал ProektOrdering🖋', callback_data='subscribe')
    bt2 = InlineKeyboardButton('💰Разместить рекламу оборудования📝', callback_data='posting')
    bt3 = InlineKeyboardButton('ℹ️Новостной канал ProektPlus🎏', url='https://t.me/proekt_plus')
    bt4 = InlineKeyboardButton('⬅️Назад', callback_data='start')
    kb = InlineKeyboardMarkup()
    kb.row(bt1)
    kb.row(bt2)
    kb.row(bt3)
    kb.row(bt4)
    return kb


async def kb_subscribe():
    bt1 = InlineKeyboardButton('💰Да, приступить к оплате✅', 
                        callback_data='subpay%s' % int(time.time()))
    bt2 = InlineKeyboardButton('⚠️Преимущества платной подписки😎', callback_data='sub_benefits')
    kb = InlineKeyboardMarkup()
    kb.row(bt1).row(bt2)
    return kb


async def kb_sub_payout(pay_id):
    pay_url = await pay_subscribe(pay_id)
    bt1 = InlineKeyboardButton('Юmoney (Яндекс.Деньги)', url=pay_url)
    bt2 = InlineKeyboardButton('Подтвердить оплату✅', callback_data='subcheck%s' % pay_id)
    bt3 = InlineKeyboardButton('⬅️Назад', callback_data='subscribe')
    kb = InlineKeyboardMarkup()
    kb.row(bt1).row(bt2).row(bt3)
    return kb


async def kb_sub_success():
    invite_link = await bot.create_chat_invite_link(chat_id=CHANNEL_ID, member_limit=1)
    bt1 = InlineKeyboardButton('Перейти на канал ProektOrdering➡️➡️', url=invite_link['invite_link'])
    bt2 = InlineKeyboardButton('Опубликовать пост на канале ProektOrdering', callback_data='posting')
    bt3 = InlineKeyboardButton('🏠Вернуться в главное меню', callback_data='start')
    kb = InlineKeyboardMarkup()
    kb.row(bt1).row(bt2).row(bt3)
    return kb


async def kb_sub_benefits():
    bt1 = InlineKeyboardButton('💰Перейти к оплате подписки', callback_data='subscribe')
    bt2 = InlineKeyboardButton('🏠Вернуться в главное меню', callback_data='start')
    kb = InlineKeyboardMarkup()
    kb.row(bt1).row(bt2)
    return kb


async def kb_post_payout(pay_id):
    pay_url = await pay_post(pay_id)
    bt1 = InlineKeyboardButton('Юmoney (Яндекс.Деньги)', url=pay_url)
    bt2 = InlineKeyboardButton('Подтвердить оплату✅', callback_data='postcheck%s' % pay_id)
    bt3 = InlineKeyboardButton('⬅️Назад', callback_data='posting')
    kb = InlineKeyboardMarkup()
    kb.row(bt1).row(bt2).row(bt3)
    return kb


async def kb_post_type():
    bt1 = InlineKeyboardButton('Резюме', callback_data='type_1')
    bt2 = InlineKeyboardButton('Вакансия', callback_data='type_2')
    bt3 = InlineKeyboardButton('Заявка на разработку проекта', callback_data='type_3')
    bt4 = InlineKeyboardButton('Заявка на разработку раздела', callback_data='type_4')
    bt5 = InlineKeyboardButton('Реклама оборудования', callback_data='type_5')
    bt6 = InlineKeyboardButton('Реклама услуг', callback_data='type_6')
    bt7 = InlineKeyboardButton('Подработка', callback_data='type_7')
    bt8 = InlineKeyboardButton('Пропустить', callback_data='type_8')
    kb = InlineKeyboardMarkup()
    kb.add(bt1, bt2).row(bt3).row(bt4).row(bt5).row(bt6).row(bt7).row(bt8)
    return kb


async def kb_skip():
    bt1 = KeyboardButton('Пропустить')
    kb = ReplyKeyboardMarkup(resize_keyboard=True)
    kb.add(bt1)
    return kb
